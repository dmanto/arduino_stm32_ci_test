// Blink led and print serial messages

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
}

unsigned count(0);
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  Serial.println(count++);
  digitalWrite(LED_BUILTIN, LOW);
  delay(900);
}

